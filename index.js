var http = require('http')
var fs = require('fs')
var cheerio = require('cheerio')

var url = 'http://xyz.ishadow.online/'
var configFile = "D:\\Shadowsocks\\gui-config.json"

function filterHtml(html) {
	var $ = cheerio.load(html)
	var items = $('.portfolio-items .col-sm-6')
 	var accountsArray = []

 	items.each(function(item) {
 		var account = $(this).find('.hover-text h4')
 		var server = account.eq(0).children('span').eq(0).text()
 		var server_port = account.eq(1).text().split("Port：")[1]
 		var password = account.eq(2).children('span').eq(0).text()
 		var method = account.eq(3).text().split("Method:")[1]
 		var accountData = {
 			server: server,
 			server_port:server_port,
 			password: password,
 			method: method,
 			remarks: "",
 			auth: false
 		}
 		if(password != undefined && 
 			password != '' && 
 			server != undefined && 
 			server != '' && 
 			server_port != undefined && 
 			server_port != '' && 
 			method != undefined && 
 			method != '' && 
 			!$(this).hasClass('ssr')) {
 			accountsArray.push(accountData)
 		} 		
 	});

 	console.log('为您找到 ' + accountsArray.length + ' 个安全有效的影梭账号')
 	return accountsArray
}

function createFile(accountsArray) {
	var config = {
		// accountsArray 就是爬取到的账号列表
		configs: accountsArray,
		
		// 以下的配置项都是我的影梭默认项，你可以根据需要自己变更
		strategy: null,
		index: 0,
		global: false,
		enabled: false,
		shareOverLan: false,
		isDefault: false,
		localPort: 1080,
		pacUrl: null,
		useOnlinePac: false,
		availabilityStatistics: false,
		autoCheckUpdate: true,
		logViewer: null
	}

	var str = JSON.stringify(config)
	fs.writeFileSync(configFile, str)
}


http.get(url, function(res) {
	console.log('正在爬取 ' + url)
	var html = ''

	res.on('data', function(data) {
		html += data
	})

	res.on('end', function() {
		var accountsArray = filterHtml(html)
		// var json = JSON.stringify(accountsArray)
		createFile(accountsArray)
	})
}).on('error', function() {
	console.log('错误： get ' + url + ' 出错')
})
