# nodejs 爬虫

这时一个我学习 nodejs 练手的项目 
nodejs 环境下的简单网页爬虫。  
可以爬取 ishadow 发布的影梭账号

## 实现说明

1. 使用 http 模块模拟 get 请求
2. 使用 cheerio 模块模拟 Jquery 选择器
3. 使用 fs 模块将数据写入 json 文件

## 运行

1. 本地安装好 nodejs 环境和 npm 
2. git clone 本项目
3. 项目目录执行 `npm install` 安装依赖
4. 根据 `注意事项` 稍作调整
5. 在命令行工具下执行 `node index.js`


## 注意s事项

1. 如果以后 ishadow 网址有变动，可以更改 url
2. configFile 是本人电脑上影梭的配置文件的路径，根据实际情况改写
3. 这是一个简单的爬虫，不要频繁使用，后果自负

## 参考

1. [cheerio API](https://www.npmjs.com/package/cheerio)
2. 慕课网——进击Node.js基础（一）——[HTTP小爬虫](http://www.imooc.com/video/7965)